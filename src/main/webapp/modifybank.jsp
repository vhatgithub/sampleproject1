<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Modify</title>
</head>
<body>

Update bank details...
<br>
<form action="Admin" method="get">

	Old Bank Name: <input name="old_bankname" type=" text" placeholder="Bank Name"><br/>
	Old Bank Branch: <input name="old_bankbranch" type=" text" placeholder="Bank Branch "><br/>
	New Bank Name: <input name="new_bankname" type=" text" placeholder="Bank Name"><br/>
	New Bank Branch: <input name="new_bankbranch" type=" text" placeholder="Bank Branch "><br/>
	<input name="update" type="submit" value="Update">
</form>
</body>
</html>