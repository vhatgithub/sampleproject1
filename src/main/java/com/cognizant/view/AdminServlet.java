package com.cognizant.view;
import com.cognizant.model.*;
import com.cognizant.controller.*;
import com.cognizant.common.*;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.LoggerFactory;

import ch.qos.logback.classic.Logger;

/**
 * Servlet implementation class AdminServlet
 */
@WebServlet("/Admin")
public class AdminServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		PrintWriter out = response.getWriter();
		
		org.slf4j.Logger logger = LoggerFactory.getLogger("com.cognizant.AdminServlet");
		logger.info("admin page loading...");
		
		String bankName;
		String bankBranch;
		
		String addbank = request.getParameter("add_bank");
		String deletebank = request.getParameter("delete");
		String updatebank = request.getParameter("update");
		
		if (addbank != null && addbank.equals("Add bank")) {
			BankBO bankBO = new BankBO();
			bankName = request.getParameter("bankname");
			bankBranch = request.getParameter("bankbranch");
			
			Bank bank = new Bank(bankName, bankBranch);
			if (bankBO.addBank(bank)) {
				logger.info("bank successfully added");
				out.println("Bank details successfully added.<br>Click here to go to <a href='index.jsp'>Home</a><br>");
			}
		} else if (deletebank != null && deletebank.equals("Delete")) {
			BankBO bankBO = new BankBO();
			bankName = request.getParameter("bankname");
			bankBranch = request.getParameter("bankbranch");
			
			Bank bank = new Bank(bankName, bankBranch);
			if (bankBO.deleteBank(bank)) {
				logger.info("bank successfully deleted");
				out.println("Bank details successfully deleted.<br>Click here to go to <a href='index.jsp'>Home</a><br>");
			}
		} else if (updatebank != null && updatebank.equals("Update")) {
			BankBO bankBO = new BankBO();
			
			String oldBankName = request.getParameter("old_bankname");
			String oldBankBranch = request.getParameter("old_bankbranch");
			String newBankName = request.getParameter("new_bankname");
			String newBankBranch = request.getParameter("new_bankbranch");
			Bank oldBank = new Bank(oldBankName, oldBankBranch);
			Bank newBank = new Bank(newBankName, newBankBranch);
			if (bankBO.updateBank(oldBank,  newBank)) {
				logger.info("bank successfully updated");
				out.println("Bank details successfully updated.<br>Click here to go to <a href='index.jsp'>Home</a><br>");
			}
		}
		
		RequestDispatcher rd1 = request.getRequestDispatcher("addbank.jsp");
		rd1.include(request, response);
		RequestDispatcher rd2 = request.getRequestDispatcher("modifybank.jsp");
		rd2.include(request, response);
		RequestDispatcher rd3 = request.getRequestDispatcher("deletebank.jsp");
		rd3.include(request, response);
		
		try {
			Connection con = DBConnection.getConnection();
			String sql = "select bankname, bankbranch from bank;";
			PreparedStatement ps = con.prepareStatement(sql);
			ResultSet rs = ps.executeQuery();
			String html = "<html>"
					+ "<head>"
					+ "</head>"
					+ "<body>"
					+ "<table>"
					+ "<tr>"
					+ "<td>Bank Name</td>"
					+ "<td>Bank Branch</td>"
					+ "</tr>";
			BankBO bankBO = new BankBO();
			List<Bank> bankList = bankBO.getBanks();
			
			for (Bank bank : bankList) {
				html += "<tr><td>"+bank.getBankName()+"</td><td>"+bank.getBankBranch()+"</td>";
			}
			html += "</table>"
					+ "</body>"
					+ "</html>";
			out.println(html);
		} catch (SQLException ex) {
			ex.printStackTrace();
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
