package com.cognizant.view;
import com.cognizant.common.*;
import com.cognizant.controller.*;
import com.cognizant.model.*;

import java.util.*;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class CustomerServlet
 */
@WebServlet("/Customer")
public class CustomerServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		PrintWriter out = response.getWriter();
		String html = "<html>"
				+ "<head>"
				+ "<title>customer</title>"
				+ "</head>"
				+ "<body>"
				+ "<h2>Add account</h2><br><br>"
				+ "<form action='User' method='get'>"
				+ "<h2>Bank Name:</h2>"
				+ "<select name='bank'>";
		BankBO bankBO = new BankBO();
		List<Bank> bankList = bankBO.getBanks();
		
		for (Bank bank : bankList) {
			html += "<option value=\""+bank.getBankName()+"\">"+bank.getBankName()+"</option>";
		}
		html += "</select>";
		html += "<h2>Account Type</h2>";
		html += "<input name='acc_type' type='radio' value='saving'> Savings<br>"
				+ "Balance On: <input name='balance' type='date' required> "
				+ "Balance Amount: <input name='amount' type='text' placeholder='Amount' required><br><br>"
				+ "<input name='acc_type' type='radio' value='fixed_deposit'> Fixed Deposit<br>"
				+ "Tenure: <input name='tenure' type='date' required>"
				+ "Amount: <input name='amount' type='text' placeholder='Amount' required>"
				+ "Due date: <input name='due_date' type='date' required><br>"
				+ "<input type='submit' value='Submit'>";
		html += "</form></body></html>";
		out.println(html);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
