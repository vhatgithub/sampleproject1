package com.cognizant.controller;

import java.util.List;

import com.cognizant.common.Bank;
import com.cognizant.model.BankDAO;

public class BankBO {
	public List<Bank> getBanks() {
		BankDAO bankDAO = new BankDAO();
		return bankDAO.retreiveBanks();
	}
	public boolean addBank(Bank bank) {
		BankDAO bankDAO = new BankDAO();
		return bankDAO.addBank(bank);
	}
	public boolean deleteBank(Bank bank) {
		BankDAO bankDAO = new BankDAO();
		return bankDAO.deleteBank(bank);
	}
	public boolean updateBank(Bank oldBank, Bank newBank) {
		BankDAO bankDAO = new BankDAO();
		return bankDAO.updateBank(oldBank, newBank);
	}
}
