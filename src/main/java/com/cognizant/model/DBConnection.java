package com.cognizant.model;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.ResourceBundle;

public class DBConnection {
	public static Connection getConnection() {
		ResourceBundle rb = ResourceBundle.getBundle("mysql");
		String url = rb.getString("url");
		String user = rb.getString("user");
		String password = rb.getString("password");
 		Connection con = null;
 		
		try {
			Class.forName("com.mysql.jdbc.Driver");
			con = DriverManager.getConnection(url, user, password);
		} catch(ClassNotFoundException ex) {
			ex.printStackTrace();
		} catch(SQLException ex) {
			ex.printStackTrace();
		}
		return con;
	}
}
