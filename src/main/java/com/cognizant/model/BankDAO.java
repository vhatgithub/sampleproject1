package com.cognizant.model;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.sql.*;
import java.util.*;


import com.cognizant.common.Bank;

public class BankDAO {
	public List<Bank> retreiveBanks() {
		Connection con = DBConnection.getConnection();
	
		List<Bank> banks = new ArrayList<Bank>();
		
		String sql = "select bankname, bankbranch from bank;";
		try {
			PreparedStatement ps = con.prepareStatement(sql);
			ResultSet rs = ps.executeQuery(); 
			
			while (rs.next()) {
				String bankName = rs.getString("bankname");
				String bankBranch = rs.getString("bankbranch");
				Bank bank = new Bank(bankName, bankBranch);
				banks.add(bank);
			}
		} catch (SQLException ex) {
			ex.printStackTrace();
		} finally {
			try {
				con.close();
			} catch (SQLException ex) {
				ex.printStackTrace();
			}
		}
		return banks;
	}
	public boolean addBank(Bank bank) {
		Connection con = DBConnection.getConnection();
		String bankName = bank.getBankName();
		String bankBranch = bank.getBankBranch();
		
		String sql = "insert into bank(bankname, bankbranch) values(?,?);";
		try {
			PreparedStatement ps = con.prepareStatement(sql);
			ps.setString(1,bankName);
			ps.setString(2, bankBranch);
			return !ps.execute(); 
		} catch (SQLException ex) {
			ex.printStackTrace();
		} finally {
			try {
				con.close();
			} catch (SQLException ex) {
				ex.printStackTrace();
			}
		}
		return false;
	}
	public boolean deleteBank(Bank bank) {
		Connection con = DBConnection.getConnection();
		String bankName = bank.getBankName();
		String bankBranch = bank.getBankBranch();
		
		String sql = "delete from bank where bankname=? and bankbranch=?";
		try {
			PreparedStatement ps = con.prepareStatement(sql);
			ps.setString(1,bankName);
			ps.setString(2, bankBranch);
			return !ps.execute(); 
		} catch (SQLException ex) {
			ex.printStackTrace();
		} finally {
			try {
				con.close();
			} catch (SQLException ex) {
				ex.printStackTrace();
			}
		}
		return false;
	}
	public boolean updateBank(Bank oldBank, Bank newBank) {
		Connection con = DBConnection.getConnection();
		String oldBankName = oldBank.getBankName();
		String oldBankBranch = oldBank.getBankBranch();
		String newBankName = newBank.getBankName();
		String newBankBranch = newBank.getBankBranch();
		
		String sql = "update bank set bankname=?, bankbranch=? where bankname=? and bankbranch=?";
		try {
			PreparedStatement ps = con.prepareStatement(sql);

			ps.setString(1,newBankName);
			ps.setString(2, newBankBranch);
			ps.setString(3,oldBankName);
			ps.setString(4, oldBankBranch);
			return !ps.execute(); 
		} catch (SQLException ex) {
			ex.printStackTrace();
		} finally {
			try {
				con.close();
			} catch (SQLException ex) {
				ex.printStackTrace();
			}
		}
		return false;
	}
}
